/* should go to Above the Fold */
/* head.core - v1.0.2 */
(function(n,t){"use strict";function r(n){a[a.length]=n}function k(n){var t=new RegExp(" ?\\b"+n+"\\b");c.className=c.className.replace(t,"")}function p(n,t){for(var i=0,r=n.length;i<r;i++)t.call(n,n[i],i)}function tt(){var t,e,f,o;c.className=c.className.replace(/ (w-|eq-|gt-|gte-|lt-|lte-|portrait|no-portrait|landscape|no-landscape)\d+/g,"");t=n.innerWidth||c.clientWidth;e=n.outerWidth||n.screen.width;u.screen.innerWidth=t;u.screen.outerWidth=e;r("w-"+t);p(i.screens,function(n){t>n?(i.screensCss.gt&&r("gt-"+n),i.screensCss.gte&&r("gte-"+n)):t<n?(i.screensCss.lt&&r("lt-"+n),i.screensCss.lte&&r("lte-"+n)):t===n&&(i.screensCss.lte&&r("lte-"+n),i.screensCss.eq&&r("e-q"+n),i.screensCss.gte&&r("gte-"+n))});f=n.innerHeight||c.clientHeight;o=n.outerHeight||n.screen.height;u.screen.innerHeight=f;u.screen.outerHeight=o;u.feature("portrait",f>t);u.feature("landscape",f<t)}function it(){n.clearTimeout(b);b=n.setTimeout(tt,50)}var y=n.document,rt=n.navigator,ut=n.location,c=y.documentElement,a=[],i={screens:[240,320,480,640,768,800,1024,1280,1440,1680,1920],screensCss:{gt:!0,gte:!1,lt:!0,lte:!1,eq:!1},browsers:[{ie:{min:6,max:11}}],browserCss:{gt:!0,gte:!1,lt:!0,lte:!1,eq:!0},html5:!0,page:"-page",section:"-section",head:"head"},v,u,s,w,o,h,l,d,f,g,nt,e,b;if(n.head_conf)for(v in n.head_conf)n.head_conf[v]!==t&&(i[v]=n.head_conf[v]);u=n[i.head]=function(){u.ready.apply(null,arguments)};u.feature=function(n,t,i){return n?(Object.prototype.toString.call(t)==="[object Function]"&&(t=t.call()),r((t?"":"no-")+n),u[n]=!!t,i||(k("no-"+n),k(n),u.feature()),u):(c.className+=" "+a.join(" "),a=[],u)};u.feature("js",!0);s=rt.userAgent.toLowerCase();w=/mobile|android|kindle|silk|midp|phone|(windows .+arm|touch)/.test(s);u.feature("mobile",w,!0);u.feature("desktop",!w,!0);s=/(chrome|firefox)[ \/]([\w.]+)/.exec(s)||/(iphone|ipad|ipod)(?:.*version)?[ \/]([\w.]+)/.exec(s)||/(android)(?:.*version)?[ \/]([\w.]+)/.exec(s)||/(webkit|opera)(?:.*version)?[ \/]([\w.]+)/.exec(s)||/(msie) ([\w.]+)/.exec(s)||/(trident).+rv:(\w.)+/.exec(s)||[];o=s[1];h=parseFloat(s[2]);switch(o){case"msie":case"trident":o="ie";h=y.documentMode||h;break;case"firefox":o="ff";break;case"ipod":case"ipad":case"iphone":o="ios";break;case"webkit":o="safari"}for(u.browser={name:o,version:h},u.browser[o]=!0,l=0,d=i.browsers.length;l<d;l++)for(f in i.browsers[l])if(o===f)for(r(f),g=i.browsers[l][f].min,nt=i.browsers[l][f].max,e=g;e<=nt;e++)h>e?(i.browserCss.gt&&r("gt-"+f+e),i.browserCss.gte&&r("gte-"+f+e)):h<e?(i.browserCss.lt&&r("lt-"+f+e),i.browserCss.lte&&r("lte-"+f+e)):h===e&&(i.browserCss.lte&&r("lte-"+f+e),i.browserCss.eq&&r("eq-"+f+e),i.browserCss.gte&&r("gte-"+f+e));else r("no-"+f);r(o);r(o+parseInt(h,10));i.html5&&o==="ie"&&h<9&&p("abbr|article|aside|audio|canvas|details|figcaption|figure|footer|header|hgroup|main|mark|meter|nav|output|progress|section|summary|time|video".split("|"),function(n){y.createElement(n)});p(ut.pathname.split("/"),function(n,u){if(this.length>2&&this[u+1]!==t)u&&r(this.slice(u,u+1).join("-").toLowerCase()+i.section);else{var f=n||"index",e=f.indexOf(".");e>0&&(f=f.substring(0,e));c.id=f.toLowerCase()+i.page;u||r("root"+i.section)}});u.screen={height:n.screen.height,width:n.screen.width};tt();b=0;n.addEventListener?n.addEventListener("resize",it,!1):n.attachEvent("onresize",it)})(window);
/* head.css3 - v1.0.0 */
(function(n,t){"use strict";function a(n){for(var r in n)if(i[n[r]]!==t)return!0;return!1}function r(n){var t=n.charAt(0).toUpperCase()+n.substr(1),i=(n+" "+c.join(t+" ")+t).split(" ");return!!a(i)}var h=n.document,o=h.createElement("i"),i=o.style,s=" -o- -moz- -ms- -webkit- -khtml- ".split(" "),c="Webkit Moz O ms Khtml".split(" "),l=n.head_conf&&n.head_conf.head||"head",u=n[l],f={gradient:function(){var n="background-image:";return i.cssText=(n+s.join("gradient(linear,left top,right bottom,from(#9f9),to(#fff));"+n)+s.join("linear-gradient(left top,#eee,#fff);"+n)).slice(0,-n.length),!!i.backgroundImage},rgba:function(){return i.cssText="background-color:rgba(0,0,0,0.5)",!!i.backgroundColor},opacity:function(){return o.style.opacity===""},textshadow:function(){return i.textShadow===""},multiplebgs:function(){i.cssText="background:url(https://),url(https://),red url(https://)";var n=(i.background||"").match(/url/g);return Object.prototype.toString.call(n)==="[object Array]"&&n.length===3},boxshadow:function(){return r("boxShadow")},borderimage:function(){return r("borderImage")},borderradius:function(){return r("borderRadius")},cssreflections:function(){return r("boxReflect")},csstransforms:function(){return r("transform")},csstransitions:function(){return r("transition")},touch:function(){return"ontouchstart"in n},retina:function(){return n.devicePixelRatio>1},fontface:function(){var t=u.browser.name,n=u.browser.version;switch(t){case"ie":return n>=9;case"chrome":return n>=13;case"ff":return n>=6;case"ios":return n>=5;case"android":return!1;case"webkit":return n>=5.1;case"opera":return n>=10;default:return!1}}};for(var e in f)f[e]&&u.feature(e,f[e].call(),!0);u.feature()})(window);
/* head.load - v1.0.3 */
(function(n,t){"use strict";function w(){}function u(n,t){if(n){typeof n=="object"&&(n=[].slice.call(n));for(var i=0,r=n.length;i<r;i++)t.call(n,n[i],i)}}function it(n,i){var r=Object.prototype.toString.call(i).slice(8,-1);return i!==t&&i!==null&&r===n}function s(n){return it("Function",n)}function a(n){return it("Array",n)}function et(n){var i=n.split("/"),t=i[i.length-1],r=t.indexOf("?");return r!==-1?t.substring(0,r):t}function f(n){(n=n||w,n._done)||(n(),n._done=1)}function ot(n,t,r,u){var f=typeof n=="object"?n:{test:n,success:!t?!1:a(t)?t:[t],failure:!r?!1:a(r)?r:[r],callback:u||w},e=!!f.test;return e&&!!f.success?(f.success.push(f.callback),i.load.apply(null,f.success)):e||!f.failure?u():(f.failure.push(f.callback),i.load.apply(null,f.failure)),i}function v(n){var t={},i,r;if(typeof n=="object")for(i in n)!n[i]||(t={name:i,url:n[i]});else t={name:et(n),url:n};return(r=c[t.name],r&&r.url===t.url)?r:(c[t.name]=t,t)}function y(n){n=n||c;for(var t in n)if(n.hasOwnProperty(t)&&n[t].state!==l)return!1;return!0}function st(n){n.state=ft;u(n.onpreload,function(n){n.call()})}function ht(n){n.state===t&&(n.state=nt,n.onpreload=[],rt({url:n.url,type:"cache"},function(){st(n)}))}function ct(){var n=arguments,t=n[n.length-1],r=[].slice.call(n,1),f=r[0];return(s(t)||(t=null),a(n[0]))?(n[0].push(t),i.load.apply(null,n[0]),i):(f?(u(r,function(n){s(n)||!n||ht(v(n))}),b(v(n[0]),s(f)?f:function(){i.load.apply(null,r)})):b(v(n[0])),i)}function lt(){var n=arguments,t=n[n.length-1],r={};return(s(t)||(t=null),a(n[0]))?(n[0].push(t),i.load.apply(null,n[0]),i):(u(n,function(n){n!==t&&(n=v(n),r[n.name]=n)}),u(n,function(n){n!==t&&(n=v(n),b(n,function(){y(r)&&f(t)}))}),i)}function b(n,t){if(t=t||w,n.state===l){t();return}if(n.state===tt){i.ready(n.name,t);return}if(n.state===nt){n.onpreload.push(function(){b(n,t)});return}n.state=tt;rt(n,function(){n.state=l;t();u(h[n.name],function(n){f(n)});o&&y()&&u(h.ALL,function(n){f(n)})})}function at(n){n=n||"";var t=n.split("?")[0].split(".");return t[t.length-1].toLowerCase()}function rt(t,i){function e(t){t=t||n.event;u.onload=u.onreadystatechange=u.onerror=null;i()}function o(f){f=f||n.event;(f.type==="load"||/loaded|complete/.test(u.readyState)&&(!r.documentMode||r.documentMode<9))&&(n.clearTimeout(t.errorTimeout),n.clearTimeout(t.cssTimeout),u.onload=u.onreadystatechange=u.onerror=null,i())}function s(){if(t.state!==l&&t.cssRetries<=20){for(var i=0,f=r.styleSheets.length;i<f;i++)if(r.styleSheets[i].href===u.href){o({type:"load"});return}t.cssRetries++;t.cssTimeout=n.setTimeout(s,250)}}var u,h,f;i=i||w;h=at(t.url);h==="css"?(u=r.createElement("link"),u.type="text/"+(t.type||"css"),u.rel="stylesheet",u.href=t.url,t.cssRetries=0,t.cssTimeout=n.setTimeout(s,500)):(u=r.createElement("script"),u.type="text/"+(t.type||"javascript"),u.src=t.url);u.onload=u.onreadystatechange=o;u.onerror=e;u.async=!1;u.defer=!1;t.errorTimeout=n.setTimeout(function(){e({type:"timeout"})},7e3);f=r.head||r.getElementsByTagName("head")[0];f.insertBefore(u,f.lastChild)}function vt(){for(var t,u=r.getElementsByTagName("script"),n=0,f=u.length;n<f;n++)if(t=u[n].getAttribute("data-headjs-load"),!!t){i.load(t);return}}function yt(n,t){var v,p,e;return n===r?(o?f(t):d.push(t),i):(s(n)&&(t=n,n="ALL"),a(n))?(v={},u(n,function(n){v[n]=c[n];i.ready(n,function(){y(v)&&f(t)})}),i):typeof n!="string"||!s(t)?i:(p=c[n],p&&p.state===l||n==="ALL"&&y()&&o)?(f(t),i):(e=h[n],e?e.push(t):e=h[n]=[t],i)}function e(){if(!r.body){n.clearTimeout(i.readyTimeout);i.readyTimeout=n.setTimeout(e,50);return}o||(o=!0,vt(),u(d,function(n){f(n)}))}function k(){r.addEventListener?(r.removeEventListener("DOMContentLoaded",k,!1),e()):r.readyState==="complete"&&(r.detachEvent("onreadystatechange",k),e())}var r=n.document,d=[],h={},c={},ut="async"in r.createElement("script")||"MozAppearance"in r.documentElement.style||n.opera,o,g=n.head_conf&&n.head_conf.head||"head",i=n[g]=n[g]||function(){i.ready.apply(null,arguments)},nt=1,ft=2,tt=3,l=4,p;if(r.readyState==="complete")e();else if(r.addEventListener)r.addEventListener("DOMContentLoaded",k,!1),n.addEventListener("load",e,!1);else{r.attachEvent("onreadystatechange",k);n.attachEvent("onload",e);p=!1;try{p=!n.frameElement&&r.documentElement}catch(wt){}p&&p.doScroll&&function pt(){if(!o){try{p.doScroll("left")}catch(t){n.clearTimeout(i.readyTimeout);i.readyTimeout=n.setTimeout(pt,50);return}e()}}()}i.load=i.js=ut?lt:ct;i.test=ot;i.ready=yt;i.ready(r,function(){y()&&u(h.ALL,function(n){f(n)});i.feature&&i.feature("domloaded",!0)})})(window);
/* loadCSS */
function loadCSS(f){for(var i=0;i<f.length;i++){loadCssFile(f[i]);}}function loadCssFile(href){var ss=window.document.createElement('link'),ref=window.document.getElementsByTagName('body')[0];ss.rel='stylesheet';ss.href=href;ss.media='only x';ref.parentNode.insertBefore(ss,ref.nextSibling);setTimeout(function(){ss.media='all';},180);}
/* Cookies */
function getCookie(c_name) { var i, x, y, ARRcookies = document.cookie.split(";"); for (i = 0; i < ARRcookies.length; i++) { x = ARRcookies[i].substr(0, ARRcookies[i].indexOf("=")); y = ARRcookies[i].substr(ARRcookies[i].indexOf("=") + 1); x = x.replace(/^\s+|\s+$/g, ""); if (x == c_name) { return unescape(y); } } }
function setCookie(c_name, value, exdays) { var exdate = new Date(); exdate.setDate(exdate.getDate() + exdays); var c_value = escape(value) + ((exdays == null) ? "" : "; expires=" + exdate.toUTCString())+"; path=/"; document.cookie = c_name + "=" + c_value; }
/* Viewport iPad */
//if(window.navigator.userAgent.match(/iPad/i) != null) { document.getElementById('viewport').setAttribute('content','width=1024,user-scalable=no'); }


var debugMode = false;


$(document).ready(function() {
    //initDebugMode();
    //c('I\'m ready & in Debugmode');
    initMenu();
    initMobileNav();
    initCssOptimizing();
    initSearch();
    initEmmaSearch();
    initPageSearch();
    initFaqs();
    initSlick();
    initFancybox();
    initYouTubePlayer();
    initEmailDecoder();
    initMoreNews();
    initTeaserboxClick();
    initWaypoints();
    initQuicklinks();
    initSectionAccordeon();
    initTimeline();
    initDateFilter();
    initPartnerFilter();
    initAdvancedTableHeights();
    initReadmore();
    //initCustomSelect();
    //init_date_links();
    $(window).resize(function(){
        if(debugMode){
            getWindowSize();
            showGridSizes();
        }
        initAdvancedTableHeights();
    });
    if($('body').hasClass('dev')){
        initSectionHelperHiding();
    }
});

function initReadmore()
{
    $('a.readmore').on('click', function(e){
        e.preventDefault();
        var id = $(this).attr('data');
        console.log(id);
        $(this).remove();
        $('div.'+id).removeClass('hide');
    })

}


function initSectionHelperHiding()
{
    //console.log('initSectionHelperHiding');
    $(document).on('keydown', function(e) {
        console.log(e.keyCode);
        if ( e.keyCode == 83 ) {
            $('.s').toggleClass('hideSectionHelper');
            $('.s:not([data-sections])').addClass('hideSectionHelper');
        }
    });
    $('.s:not([data-sections])').addClass('hideSectionHelper');
}

function initAdvancedTableHeights()
{
    $('.advanced_table').each(function(){
        var r_c1 = 0;
        var atable = $(this);
        $(this).find('.innerRow').removeAttr('style');
        var windowSize = $(window).outerWidth();
        if(windowSize > 992){
            $(this).find(' .outerC_1').each(function(){
                $(this).find(' .innerRow').each(function(){
                    var cheight = $(this).outerHeight();
                    var data = $(this).attr('data');
                    var corresponding_row_height = atable.find('.outerC_2 .'+data).outerHeight();
                    if(cheight > corresponding_row_height){
                        atable.find('.outerC_2 .'+data).css('height', cheight + 'px');
                    }else{
                        atable.find('.outerC_1 .'+data).css('height', corresponding_row_height + 'px');
                    }
                })
            })
        }
    })
}


function initQuicklinks()
{
    $('.quicklinks a').click(function(){
        $('.quicklinks a').removeClass('active');
        $(this).addClass('active');
    })
}

function init_date_links(){
    $('.date_event_list.link').each(function(){
        var link = $(this).attr('data-link');
        if(link != ''){
            $(this).on('click', function(){
                document.location = link;
            })
        }
    })
}

function initCustomSelect()
{
    // Iterate over each select element
    $('select').each(function () {

        // Cache the number of options
        var $this = $(this),
            numberOfOptions = $(this).children('option').length;

        // Hides the select element
        $this.addClass('s-hidden');

        // Wrap the select element in a div
        $this.wrap('<div class="select"></div>');

        // Insert a styled div to sit over the top of the hidden select element
        $this.after('<div class="styledSelect"></div>');

        // Cache the styled div
        var $styledSelect = $this.next('div.styledSelect');

        // Show the first select option in the styled div
        $styledSelect.text($this.children('option').eq(0).text());

        // Insert an unordered list after the styled div and also cache the list
        var $list = $('<ul />', {
            'class': 'options'
        }).insertAfter($styledSelect);

        // Insert a list item into the unordered list for each select option
        for (var i = 0; i < numberOfOptions; i++) {
            $('<li />', {
                text: $this.children('option').eq(i).text(),
                rel: $this.children('option').eq(i).val()
            }).appendTo($list);
        }

        // Cache the list items
        var $listItems = $list.children('li');

        // Show the unordered list when the styled div is clicked (also hides it if the div is clicked again)
        $styledSelect.click(function (e) {
            e.stopPropagation();
            $('div.styledSelect.active').each(function () {
                $(this).removeClass('active').next('ul.options').hide();
            });
            $(this).toggleClass('active').next('ul.options').toggle();
        });

        // Hides the unordered list when a list item is clicked and updates the styled div to show the selected list item
        // Updates the select element to have the value of the equivalent option
        $listItems.click(function (e) {
            e.stopPropagation();
            $styledSelect.text($(this).text()).removeClass('active');
            $this.val($(this).attr('rel'));
            $list.hide();
            /* alert($this.val()); Uncomment this for demonstration! */
        });

        // Hides the unordered list when clicking outside of it
        $(document).click(function () {
            $styledSelect.removeClass('active');
            $list.hide();
        });

    });
}

function initPartnerFilter()
{
    $('select.partnerfilter').on('change', function(){
        var filter = $(this).val();
        var name_selection = $(this).attr('var-select');
        var name_deselection = $(this).attr('var-deselect');
        console.log(name_selection + ' - ' + name_deselection);
        //console.log(filter + ' ' + name_selection + ' ' + name_deselection);
        if(filter == 'none'){
            $(this).find('option:first').html(name_selection);
        }else{
            $(this).find('option:first').html(name_deselection);
        }

        //console.log($(this).val());
        updatePartnerFilterView();
    })
}

function updatePartnerFilterView()
{
    let filter1 = $('select.partnerfilter[name="partnerfilter_1"]').val() || 'all';
    let filter2 = $('select.partnerfilter[name="partnerfilter_2"]').val() || 'all';
    let filter3 = $('select.partnerfilter[name="partnerfilter_3"]').val() || 'all';

    console.log( filter1 + filter2 + filter3);

    var filter = '';
    if(filter1 !== 'all' && filter1 !=='none'){
        filter += '.'+filter1;
    }
    if(filter2 !== 'all' && filter2 !=='none'){
        filter += '.'+filter2;
    }
    if(filter3 !== 'all' && filter3 !=='none'){
        filter += '.'+filter3;
    }
    //console.log(filter);
    $('.partnerfilterelement').addClass('pfhide');
    $('.partnerfilterelement' + filter).removeClass('pfhide');

}

function initWaypoints(){

    $('.lazyimg').waypoint(function() { $(this.element).loadLazy(); this.destroy(); },{offset:'140%'});
    setTimeout(function() { Waypoint.refreshAll(); },3000);

    $('.anchortag').waypoint(function() { $(this.element).changeQuickmenuLinks();  },{offset:'100%'});
    $('.quicklinks').waypoint(function() { $(this.element).fixQuickmenu();  },{offset:'0%%'});
    $('.quicklinks').waypoint(function() { $(this.element).unfixQuickmenu();  },{offset:'100px'});
    //setTimeout(function() { Waypoint.refreshAll(); },3000);
}

$.fn.loadLazy = function() {
    this.each(function() {
        if($(this).attr('data-pic') != undefined){
            var pic = $(this).attr('data-pic');
            var picmobile = $(this).attr('data-pic-mobile');
            if (head.screen.innerWidth < 700 && picmobile) {
                $(this).attr('src', picmobile);
            } else if (pic) {
                $(this).attr('src', pic);
            }
            $(this).removeAttr('data-pic');
            $(this).removeAttr('data-pic-mobile');
        }

    });
};
$.fn.fixQuickmenu = function() {
    this.each(function() {
        $(this).addClass('fixed');
    });
};
$.fn.unfixQuickmenu = function() {
    this.each(function() {
        $(this).removeClass('fixed');
    });
};
$.fn.changeQuickmenuLinks = function() {
    this.each(function() {
        if($(this).attr('id') != undefined){
            var name = $(this).attr('id');
            console.log(name);
            $('.quicklinks a').removeClass('active');
            $('.quicklinks a[data-tagname="'+name+'"]').addClass('active');
        }

    });
};

/* Init Menü Function */
function initMenu()
{




    // Click outside of submenu closes submenu
    /*$(document).on('click', function(e){
        if (!document.getElementById('mn').contains(e.target)) {
            // close submenu
            console.log(e.target);
            $('#mn a.mn_active').removeClass('mn_active');
            closeHeaderSearchBar();
        }
    });
    // Open/Close Submenu on Menuitemclick
    $('#mn > li > a').on('click', function(e){
        //c($(this).parent().hasClass('menu-item-has-children'));
        if($(this).parent().hasClass('menu-item-has-children')){
            //e.preventDefault();
        }

        // If submenu is already open, close submenu
        /*if($(this).hasClass('mn_active')){
            $(this).toggleClass('mn_active');
        }else{
            // close all submenus and open the clicked one
            $('#mn > li.menu-item-has-children > a').removeClass('mn_active');
            $(this).toggleClass('mn_active');
        }*/
        // close searchbar
  //      closeHeaderSearchBar();
   // });


}



/* Opening & Closing mobile Navigation*/
function initMobileNav()
{
    c('initMobileNav');
    $('#mobilenav').on('click',function(){
        $('#mn').slideToggle();
        if($('#mobilenav .far').hasClass('fa-bars')){
            $('#mobilenav .far').removeClass('fa-bars').addClass('fa-times');
        } else {
            $('#mobilenav .far').removeClass('fa-times').addClass('fa-bars');
        }
    });

    // Add Folder Icon Element & Toggle */
    $('#mn li.menu-item-has-children > a').not('#mn li li.menu-item-has-children > a').after('<i class="fal fa-angle-down mn_foldericon"></i>');
    $('#mn').on('click','.fal',function(){
        $(this).parent().toggleClass('mobile_nav_parent');
        $(this).toggleClass('fa-angle-up fa-angle-down');
        $(this).next().slideToggle();
    });

    // Listen for orientation changes & reset styles
    window.addEventListener("resize", function() {
        $('#header').removeAttr('style');
        $('#header').removeClass('fixed');
        var directStyle = $('#mn').attr('style');
        if (typeof directStyle !== 'undefined' && directStyle !== false) {
            directStyle = directStyle.replace('display: none;', '');
            directStyle = directStyle.replace('display:none;', '');
            $('#mn').attr('style', directStyle);
        }
        $('#header #mn .sub-menu').removeAttr('style');
    }, false);
}



// Adds Classes to HTML-DOM for advanced Fluid Design
function initCssOptimizing()
{
    fixedNavigation();
    /* remove padding on textboxelements with only one ul inside*/
    $('.rtextbox .textbox .content ul:only-child').closest('.textbox').addClass('onlyUl')
}
var lastScrollTop = 0;
function fixedNavigation() {
    $(window).scroll(function (event) {
        if (head.screen.innerWidth > 786) {
            var st = $(this).scrollTop();
            //c(st);
            if (st > lastScrollTop) {
                // downscroll code
                $('#header').removeClass('fixed');
                $('body').removeClass('fixedheader');
            } else {
                // upscroll code
                if (st > 130) {
                    $('#header').addClass('fixed');
                    $('body').addClass('fixedheader');
                } else if (st == 0) {
                    $('#header').removeClass('fixed');
                    $('body').removeClass('fixedheader');
                }
            }
            lastScrollTop = st;
        }
    });
}




/* Search Functions */
function initSearch()
{

    // Click outside of headersearch closes searchbar
    document.addEventListener('click', function(e){
        if (document.getElementById('searchform').contains(e.target)){
            // Clicked in box
            //console.log('You clicked inside');
        } else{
            //console.log('You clicked outside');
            closeHeaderSearchBar();
            emptySearchbar();
        }
    });


    // Searchbar click
    $('.searchbox').on('click', function(){
        $('.searchbox').addClass('sb_active');
    })
    $('#headersearch .btn_close_search').on('click', function(e){
        // close Search
        //e.stopPropagation();
        closeHeaderSearchBar();
        emptySearchbar();
    })


    // Searchfunctionality
    let timeout = null;
    $('.headersearch input').on('keydown', function (e) {
        //console.log(e.keyCode);


        var element = $('.headersearch .awesomeplete ul');
        if(is_suggest_open(element)){
            $('.headersearch').addClass('sb_suggest');
        }else{
            $('.headersearch').removeClass('sb_suggest');
        }


        // Only start the request if it is a real Keyinput not Arrow Keys
        //https://www.freecodecamp.org/news/javascript-keycode-list-keypress-event-key-codes/
        if(e.keyCode > 46 && e.keyCode <= 91 || e.keyCode == 8 ){
            // Timeout for limited request per second
            clearTimeout(timeout);
            var inputField = $(this);




            timeout = setTimeout(function () {
                var input = inputField.val();
                console.log(input);
                if(input.length >=2){
                    var response = getStrainSearchResults(input, inputField );
                    //console.log("response "+response);
                    if(response !== ''){
                        $('.awesomplete ul').removeAttr('hidden');
                    }
                    //c('html: '+$('#headerseach .awesomplete ul:first-child').html());
                    //c('html ende');
                    if($('.awesomplete ul').html() == ''){
                        $('.awesomplete ul').attr('hidden', 'hidden');
                    }else{

                    }

                }
            }, 200);
        }

        // Enter or Return => goto Searchresultpage
        if(e.charCode == 13){
            window.location.href = "/?s="+input;
        }
    });

    // Mouseclick on Search Icon starts search
    $('.headersearch i.fa-search').on('click', function(e){
        e.preventDefault();
        $('#searchform').submit();
    });

    // Click on suggest
    $(document).on('mousedown', 'body .awesomplete ul li', function(e){
        //let keyword = $(this).closest('form').find('input').val();
        //console.log('Keyword: ' + keyword);
        if($(this).hasClass('strainresultlistelement')){
            // Go to StrainResultlist
            const cat = $(this).attr('data-category');
            const keyword = $(this).attr('data-keyword');
            const url = '/strain-search/?category='+cat+'&keyword='+keyword;
            $(this).closest('input').val(keyword);
            window.location.href = url;
        }else if($(this).hasClass('pageresults')){
            // Go to Pagesearch
            const keyword = $(this).attr('data-keyword');
            const url = '/?s='+keyword;
            $('#pagesearch').val(keyword);
            window.location.href = url;
        }else{
            // Goto Search Results
            $(this).closest('form').submit();
        }

        /*return '';
        if($(this).hasClass('strainresultlistelement')){
            // Goto StrainResultlist
            //const url = 'https://www.infrafrontier.eu/search?category=diseases&keyword=';
            const url = '/strain-search/?category=all&keyword=';
            window.location.href = url + 'keyword';
        }else{
            // Goto Search Results
            window.location.href = '/?s='+keyword;
        }*/
    })
}

function is_suggest_open(element)
{
    var hidden = element.attr('hidden');
    //console.log('suggest_open: ' + hidden);
    //console.log(element);
    if(hidden == ''){
        return true;
    }else{
        return false;
    }

}



function closeHeaderSearchBar()
{
    //console.log('closeHeaderSearchBar');
    $('.searchbox').removeClass('sb_active');
    $('.searchbox input').val('');
    hideSuggestBox();
}
function hideSuggestBox()
{
    $('#searchform').closest('.awesomeplete ul').attr('hidden', '');
    //$('.awesomeplete ul').attr('hidden', '');
    emptySearchbar();
}

function emptySearchbar()
{
    //console.log('emptySearchbar');
    $('.headersearch').removeClass('sb_suggest');
    $('.searchbox .awesomplete ul').html('');
    $('.searchbox .awesomplete ul').attr('hidden', 'hidden');

    $('.awesomeplete ul').html(' ');
}




function getStrainSearchResults(query, inputField)
{
    const data = {action: 'getStrainSearchData', keyword: query};
    //console.log(dr_strainsearchurl);
    $.ajax({
        type: "POST",
        url: dr_strainsearchurl, // set in header.php
        data: data,
        success: function(response){
            //console.log(response);
            $('.strainresultlist').remove();
            if(response != ''){
                inputField.parent().find('ul').append(response);
                inputField.parent().find('ul').removeAttr('hidden');
            }

            return response;
        },
        error: function(XMLHttpRequest, textStatus, errorThrown) {
            strainRequestRunning = false;
            console.log('Stains could not get loaded', XMLHttpRequest, textStatus, errorThrown);
        }
    });
}


function getEmmaStrainSearchResults(query, success_function, showStrainHeadline = false)
{
    const data = {action: 'getStrainSearchData', keyword: query};
    //console.log(dr_strainsearchurl);
    $.ajax({
        type: "POST",
        url: dr_strainsearchurl, // set in header.php
        data: data,
        success: function(response){
            if(!showStrainHeadline){
                response = response.replace('<h4>Strain search</h4>','');
            }
            success_function(response);
        },
        error: function(XMLHttpRequest, textStatus, errorThrown) {
            console.log('Stains could not get loaded', XMLHttpRequest, textStatus, errorThrown);
        }
    });
}


function initPageSearch()
{
    // Searchfunctionality
    let pagesearchtimeout = null;

    $('input#pagesearch').on('keydown', function (e) {
        // Only start the request if it is a real Keyinput not Arrow Keys
        //https://www.freecodecamp.org/news/javascript-keycode-list-keypress-event-key-codes/
        if(e.keyCode > 46 && e.keyCode <= 91 || e.keyCode == 8 ){
            // Timeout for limited request per second
            clearTimeout(pagesearchtimeout);
            var inputField = $(this);

            pagesearchtimeout = setTimeout(function () {
                var input = inputField.val();
                if(input.length >=2){
                    var response_page = getPageSuggestList(input);
                    if(response_page != ''){
                        $('.rpagesearch .awesomplete ').html(response_page);
                        $('.rpagesearch').addClass('sb_suggest');
                    }
                    //var response = getStrainSearchResults(input, inputField );
                    var response = getEmmaStrainSearchResults(input, pagesearchResult, true);
                }else{
                    $('.rpagesearch ').removeClass('sb_suggest');
                    $('.rpagesearch  .awesomplete ').html('');

                }
            }, 200);
        }

        if(e.keyCode == 13){
            window.location.href = "/?s="+$(this).val();
        }

    });

    // Hide Suggest on unfocus
    $('#pagesearch').on('focusout', function(){
        $('.rpagesearch  .awesomplete ').html('');
        $('.rpagesearch ').removeClass('sb_suggest');
    })
}

function getPageSuggestList(input)
{
    var resultlist = '';
    const list = awesomplete._list;
    // Get all entries that match the search
    const matches = list.filter(s => s.includes(input));

    // Sort by length and ABC
    matches.sort(function(a, b){
        return a.length - b.length || a.localeCompare(b);
    });

    // Generate Resultlist
    resultlist += '<ul class="pageresults">';
    for(var i = 0 ; (i < matches.length && i <6); i++){
        var result = matches[i];
        var result_highlighted = result.replace( input , '<strong>'+ input +'</strong>');
        resultlist += '<li data-keyword="'+ result +'" class="pageresults">' + result_highlighted + '</li>';
    }
    resultlist += '</ul>';
    return resultlist;
}


function pagesearchResult(response)
{
    if(response != ''){
        console.log("Response given");
        //$('.rpagesearch .awesomplete ').html(response);
        $('.rpagesearch .awesomplete ').append(response);

        $('.rpagesearch').addClass('sb_suggest');
    }else{
        console.log("no Response given");
        $('.rpagesearch .awesomplete ').html('');
        $('.rpagesearch').removeClass('sb_suggest');
    }
}



function initEmmaSearch()
{

    //console.log('initEmmaSearch');
    // Searchfunctionality
    let emmatimeout = null;
    $('input#emmastrainsearch').on('keydown', function (e) {

        // Only start the request if it is a real Keyinput not Arrow Keys
        //https://www.freecodecamp.org/news/javascript-keycode-list-keypress-event-key-codes/
        if(e.keyCode > 46 && e.keyCode <= 91 || e.keyCode == 8 ){
            // Timeout for limited request per second
            clearTimeout(emmatimeout);
            var inputField = $(this);

            emmatimeout = setTimeout(function () {
                var input = inputField.val();
                //console.log("Input: " + input);
                if(input.length >=2){
                    //var response = getStrainSearchResults(input, inputField );
                    var response = getEmmaStrainSearchResults(input, emmaStrainSearchResult);
                }else{
                    $('.remmastrainsearch').removeClass('sb_suggest');
                    $('.remmastrainsearch .awesomplete ').html('');

                }
            }, 200);
        }

        if(e.keyCode == 13){
            window.location.href = "/strain-search/?category=all&keyword="+$(this).val();
        }

    });

    $('#emmastrainsearch').on('focusout', function(){
        $('.remmastrainsearch .awesomplete ').html('');
        $('.remmastrainsearch').removeClass('sb_suggest');
    })
}

function emmaStrainSearchResult(response)
{
    if(response != ''){
        console.log("Response given");
        $('.remmastrainsearch .awesomplete ').html(response);
        $('.remmastrainsearch').addClass('sb_suggest');
    }else{
        $('.remmastrainsearch .awesomplete ').html('');
        $('.remmastrainsearch').removeClass('sb_suggest');
    }
}










/* FAQs */
function initFaqs()
{
    $('.faq .question, .faq .icon').click(function(){
        $(this).parent().toggleClass('show');
        $(this).parent().find('.answer').slideToggle();
    })
}

function initSectionAccordeon()
{
    $('.raccordeoncontent .s').each(function(){
        if($(this).hasClass('size_2')){
            $(this).parent().parent().parent().parent().find('.accordeontitle').addClass('size_2');
        }else if($(this).hasClass('size_3')){
            $(this).parent().parent().parent().parent().find('.accordeontitle').addClass('size_3');
        }
    });
    $('.accordeon_title').click(function(){
        $(this).parent().parent().parent().toggleClass('show');
        $(this).parent().parent().parent().parent().find('.raccordeoncontent').slideToggle();
        //$(this).parent().parent().parent().parent().find('.raccordeoncontent').toggleClass('show');
    })
}


function initTeaserboxClick()
{
    $('.teaserboxcontainer').on('click', function(e){
        var link = $(this).find('a').first().attr('href');
        window.location.href = link;
    })
    $('.sidebarcontainer').on('click', function(e){
        var link = $(this).find('a').first().attr('href');
        window.location.href = link;
    })

    $('.teaserboxcontainer').on('auxclick', function(){
        var link = $(this).find('a').first().attr('href');
        window.open(
            link,
            '_blank'
        );
    })
}

/* Slick Slider for Carousel */
function initSlick()
{

    c("initSlick");

    $('.slickslider').slick({
        slidesToShow: 4,
        slidesToScroll: 4,
        autoplay: false,
        //autoplaySpeed: 5000,
        centerMode: true,
        variableWidth: false,
        adaptiveHeight: false,
        dots: false,
        arrows: true,
        focusOnSelect: false,
        infinite: true,
        prevArrow	   : '<div class="arrow prev shadow-basic"><i class="fal fa-chevron-left"></i></div>',
        nextArrow      : '<div class="arrow next shadow-basic"><i class="fal fa-chevron-right"></i></div>',
        responsive: [
            {
                breakpoint: 750,
                settings: {slidesToShow: 1, slidesToScroll: 1}
            },
            {
                breakpoint: 800,
                settings: {slidesToShow: 2, slidesToScroll: 2}
            },
            {
                breakpoint: 1024,
                settings: {slidesToShow: 3, slidesToScroll: 3}
            }
        ]
    });

    $('.logoslider').slick({
        slidesToShow: 4,
        slidesToScroll: 4,
        autoplay: false,
        //autoplaySpeed: 5000,
        centerMode: true,
        variableWidth: true,
        adaptiveHeight: false,
        dots: false,
        arrows: true,
        focusOnSelect: false,
        infinite: true,
        prevArrow	   : '<div class="arrow prev shadow-basic"><i class="fal fa-chevron-left"></i></div>',
        nextArrow      : '<div class="arrow next shadow-basic"><i class="fal fa-chevron-right"></i></div>',
        responsive: [
            {
                breakpoint: 750,
                settings: {slidesToShow: 1, slidesToScroll: 1}
            },
            {
                breakpoint: 800,
                settings: {slidesToShow: 2, slidesToScroll: 2}
            },
            {
                breakpoint: 1024,
                settings: {slidesToShow: 3, slidesToScroll: 3}
            }
        ]
    });


    $('.imageslider').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        autoplay: false,
        //autoplaySpeed: 5000,
        centerMode: false,
        variableWidth: false,
        adaptiveHeight: false,
        dots: true,
        arrows: true,
        focusOnSelect: false,
        infinite: true,
        prevArrow	   : '<div class="arrow prev shadow-basic"><i class="fal fa-chevron-left"></i></div>',
        nextArrow      : '<div class="arrow next shadow-basic"><i class="fal fa-chevron-right"></i></div>'

    });

    $('.quote-slider').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        autoplay: false,
        //autoplaySpeed: 5000,
        centerMode: false,
        variableWidth: false,
        adaptiveHeight: true,
        dots: true,
        arrows: true,
        focusOnSelect: false,
        infinite: true,
        prevArrow	   : '<div class="arrow prev shadow-basic"><i class="fal fa-chevron-left"></i></div>',
        nextArrow      : '<div class="arrow next shadow-basic"><i class="fal fa-chevron-right"></i></div>'

    });


    $('.timelinenavigation').slick({
        slidesToShow: 5,
        slidesToScroll: 1,
        autoplay: false,
        //autoplaySpeed: 5000,
        centerMode: false,
        variableWidth: false,
        adaptiveHeight: false,
        dots: false,
        arrows: false,
        focusOnSelect: false,
        infinite: false,
        swipe: true,
        responsive: [
            {
                breakpoint: 750,
                settings: {slidesToShow: 1, slidesToScroll: 1}
            },
            {
                breakpoint: 800,
                settings: {slidesToShow: 2, slidesToScroll: 1 }
            },
            {
                breakpoint: 1024,
                settings: {slidesToShow: 5, slidesToScroll: 1}
            }
        ]
    });

    // Bug umgehen und Slick reinitialisieren (geht nur bei slicks, die nur eine Row haben)
    /*
     setTimeout(function () {
     $('.slickslider').slick('unslick').slick('reinit');
     }, 1500);*/

    // Postslider
    /*if (head.screen.innerWidth >= 768) {
     $('.postslider').each(function () {
     var reihen = 1;
     if ($(this).hasClass('tworows')) {
     reihen = 2;
     }
     $(this).slick({
     rows: reihen,
     slidesToScroll: 1,
     slidesToShow: 1,
     centerMode: false,
     variableWidth: false,
     dots: true,
     arrows: false,
     responsive: [
     {
     breakpoint: 750,
     settings: {slidesToShow: 1, slidesToScroll: 1}
     },
     {
     breakpoint: 1350,
     settings: {slidesToShow: 2, slidesToScroll: 1}
     },
     {
     breakpoint: 3000,
     settings: {slidesToShow: 3, slidesToScroll: 2}
     }
     ]
     });
     });
     } else {
     $('.postslider').slick({
     rows: 1,
     slidesToScroll: 1,
     slidesToShow: 1,
     centerMode: true,
     dots: false,
     arrows: false,
     variableWidth: false,
     });
     }
     */
}

/* Fancybox for big images */
function initFancybox() {
    c('initFancybox');
    $('[data-fancybox]').fancybox();
}
/***************************
 *
 * Functions for Debugging
 * while working on the site
 *
 ***************************/

function initDebugMode()
{
    toggleDebugMode(); // If standard should be on
    getWindowSize();
    $(document).on('keydown', function(e){
        //c(e.keyCode);
        if(e.ctrlKey == true && e.keyCode == 160 && debugMode == false){
            toggleDebugMode();
        }
        if(debugMode){
            $('.keyinfos .keycode').html(e.keyCode);
            switch(e.keyCode)
            {
                case 27: // ESC
                    toggleDebugMode();
                    break;
                case 49: // 1
                    toggleGrid();
                    break;
                case 50: // 2
                    toggleColors();
                    break;
                case 51: // 3
                    toggleScreenSize();
                    break;
                case 52: // 4
                    toggleDevcomment();
                    break;
            }
        }
    });
}
function toggleDebugMode()
{
    debugMode = (debugMode == false)? true : false;
    $('.debuginfo').toggleClass('hide');
}
function toggleGrid()
{
    $('.debuggrid').toggleClass('hide');
    $('.keyinfos .grid').toggleClass('hide');
}
function toggleColors()
{
    $('.main').toggleClass('showBlockColors');
    $('.keyinfos .blockcolors').toggleClass('hide');
}
function toggleScreenSize()
{
    getWindowSize();
    $('.screen_size_display').toggleClass('hide');
    $('.keyinfos .screensize').toggleClass('hide');
}
function toggleDevcomment()
{
    $('.devcommentwrapper').toggleClass('hide');
}
function showGridSizes()
{
    $('.r.rgrid .c > div').each(function(){
        var divWidth = parseFloat($(this).outerWidth());
        //console.log(divWidth);
        $(this).html(divWidth.toFixed(1));
    });

    var sizeR = $('.sizer').closest('.r').width();
    $('.sizer').html(sizeR);
}
function getWindowSize()
{
    var windowSize = $(window).outerWidth();
    $('.windowsize').html(windowSize);
}
function c(whatever)
{
    if(debugMode){
        console.log(whatever);
    }
}

function initYouTubePlayer()
{
    $('.youtubebox .playicon').click(function(){
        $('.youtubebox .previewImage').addClass('hide');
        $('.youtubebox .youtubeplayercontainer').each(function(){
            var videosource = $(this).find('iframe').attr('data-src');
            videosource = videosource.replace('http:', '');
            videosource = videosource.replace('https:', '');
            console.log(videosource);
            $(this).find('iframe').attr('src', videosource);
        });

        $('.youtubebox .youtubeplayercontainer').removeClass('hide');
    })
}

function initEmailDecoder(){
    $('.emaillink').each(function(){
        //console.log($(this).attr('href'));
        var mailto = rot13($(this).attr('href'));
        $(this).attr('href', 'mailto:'+mailto);
        $(this).html(mailto);
        //console.log('-> '+ $(this).attr('href'));
    });
    $('.mailto.rot').each(function(){
        //console.log($(this).attr('href'));
        var href= rot13($(this).attr('href'));
        var text= rot13($(this).html());
        $(this).attr('href', href);
        $(this).html(text);
        //console.log('-> '+ $(this).attr('href'));
    });
}
function rot13(s) {
    return s.replace(/[A-Za-z]/g, function (c) {
        return "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz".charAt(
            "NOPQRSTUVWXYZABCDEFGHIJKLMnopqrstuvwxyzabcdefghijklm".indexOf(c)
        );
    } );
}


function initMoreNews()
{
    $('.loadmorenews').on('click', function(e){
        e.preventDefault();
        var counter = 0;
        var showmoreCount = $(this).attr('data-showmore');
        //$(this).parent().addClass('hide');
        $(this).parent().remove();
        $('.newsentrys .newsentry.hide').each(function(){
            if(counter < showmoreCount){
                $(this).removeClass('hide');
            }
            counter++;
        })
        $('.morewrapper.hide').first().removeClass('hide');
        //$('.morewrapper.hide').removeClass('hide');
    })
}




/* Timeline */

var timelineCounter = new Object();
function initTimeline()
{
    //console.log('initTimeline');
    $('.timeline').each(function(){
        var id = $(this).attr('id');
        timelineCounter[id] = 0;
        //console.log("Timeline with id: " + id + " initialized" );
        $('#'+id+' .timeframe').first().addClass('active');
        $('#'+id+' .timelinecontrols .next').on('click', function(){
            nextTimeframe(id);
        });
        $('#'+id+' .timelinecontrols .prev').on('click', function(){
            prevTimeframe(id);
        });
        $('#'+id+' .timelinenavigation  .timenavelement[data-count="0"]').addClass('active');
        positionTimelineLine(id);
        $('#'+id+' .timelinenavigation').on('afterChange', function(event, slick, direction){
            //console.log('after change was hit')
            positionTimelineLine(id);
        });
    });
    $('.timeline .timelinenavigation').on('click', '.timenavelement', function(){
        var id = $(this).attr('data-identifier');
        var element_pos = $(this).attr('data-count');
        timelineCounter[id] = element_pos;
        updateTimeline(id);
        positionTimelineLine(id);
    });
}

function nextTimeframe(id)
{
    var max = $('#'+id).attr('data-max');
    if(timelineCounter[id] < (max-1)){
        timelineCounter[id]++;
        updateTimeline(id);
        $('#'+id + ' .timelinenavigation ').slick('slickNext');
    }
}

function prevTimeframe(id)
{
    if(timelineCounter[id] > 0) {
        timelineCounter[id]--;
        updateTimeline(id);
        $('#'+id + ' .timelinenavigation ').slick('slickPrev');
    }
}
function updateTimeline(id)
{
    //console.log(id);
    //console.log(timelineCounter);
    $('#'+id + ' .timeframe').removeClass('active');
    $('#'+id + ' .timeframe:eq('+timelineCounter[id]+')').addClass('active');

    $('#'+id + ' .timenavelement').removeClass('active').removeClass('visited');
    //$('#'+id + ' .timenavelement i').removeClass('fas').removeClass('visited').addClass('far');
    $('#'+id + ' .timenavelement[data-count="'+timelineCounter[id]+'"]').addClass('active');
    for(var i=0; i < timelineCounter[id];i++){
        $('#'+id + ' .timenavelement[data-count="'+i+'"]').addClass('visited');
    }
    $('#'+id + ' .timenavelement[data-count="'+timelineCounter[id]+'"] i').removeClass('far').addClass('fas');
    //positionTimelineLine(id);
}

function positionTimelineLine(id)
{
    var pos = $('#'+id + ' .timelinenavigation').first().position();
    var posy = pos.top;
    var posActive = $('#'+id+' .timelinenavigation .active').position();
    var deltaSlick = $('#' +id +' .slick-track').position();
    var posx = posActive.left + deltaSlick.left ;
    //console.log(deltaSlick.left)
    //console.log(posy);
    $('#'+id + ' .timeline-line').css({"top":"calc("+posy+" + 27px)"});
    $('#'+id + ' .timeline-line.display').css({"width":"calc("+posx+"px + 80px)"});
}

function initDateFilter()
{
    $('.datefilter').each(function(){
        $(this).on('change', function(e){
            let filterType = this.value;
            $('.date_event_list').addClass('hidden');
            if(filterType == 'all'){
                $('.date_event_list').removeClass('hidden');
            }else{
                $('.date_event_list:first').removeClass('hidden');
                $('.date_event_list[data-category="'+filterType+'"').removeClass('hidden');
            }



        })
    });
}




